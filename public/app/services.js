cdApp.service('BandService', function($http) {
	return {
		getBands: function () {
			return $http.get('/bands');
		},
		getBand: function(name) {
			return $http.get('/bands/band?name=' + name);
		}
	};
});