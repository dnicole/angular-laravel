cdApp.controller('BandListController', ['$scope', 'BandService', '$q', function($scope, BandService, $q) {
	BandService.getBands().then(function(bands) {
		$scope.bandList = bands.data;
	});
}]);