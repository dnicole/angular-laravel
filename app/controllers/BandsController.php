<?php

class BandsController extends BaseController {
	public function getIndex() {
		$bands = array(
			array(
				'name' => 'Fight Plan',
				'albums' => array(
					array(
						'title' => 'Pacemaker',
						'price' => 10
					),
					array(
						'title' => 'April is Awesome',
						'price' => 5.75
					)
				),
			),
			array(
				'name' => 'Riverside Riot',
				'albums' => array(
					array(
						'title' => 'ATX',
						'price' => 8
					)
				)
			),
			array(
				'name' => 'Shitty Advice',
				'albums' => array(
					array(
						'title' => 'There Suki Goes',
						'price' => 10
					)
				)
			)
		);
		
		return Response::json($bands);
	}

	public function getBand() {
		return Input::get('name');
	}
}
