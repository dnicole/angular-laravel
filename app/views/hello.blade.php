<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>
</head>
<body ng-app="cdApp">
	<div ng-controller="BandListController">
		<h1>Band List</h1>
		<div>
			Filter by: <input type="text" ng-model="query"/>
		</div>
		<div ng-repeat="band in bandList">
			<h2><% band.name | uppercase %></h2>
			<div ng-repeat="album in band.albums | filter:query">
				<div>Title: <% album.title %></div>
				<div>Price: <% album.price | currency %></div>
				<hr />
			<div>
		</div>
	</div>
	<script type="text/javascript" src="{{ URL::asset('app/app.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('app/services.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('app/filters.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('app/controllers.js') }}"></script>
</body>
</html>
